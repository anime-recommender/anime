package handler

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github/epith/302-backend-user/bob"
	"github/epith/302-backend-user/db"
	"github/epith/302-backend-user/models"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

type animeContextKey string

type CollaborativeRecommendation struct {
	Data Collaborative `json:"data"`
}

type Collaborative struct {
	CollaborativeRecommendations []int  `json:"collaborative_recommendations"`
	UserID                       string `json:"user_id"`
}

type ContentRecommendation struct {
	Data Content `json:"data"`
}

type Content struct {
	CollaborativeRecommendations []int  `json:"content_recommendations"`
	UserID                       string `json:"user_id"`
}

type AnimeWithGenre struct {
	Anime  bob.Anime      `json:"anime,omitempty"`
	Genres bob.GenreSlice `json:"genres,omitempty"`
}

var animeIDKey = animeContextKey("id")
var animeNKey = animeContextKey("n")

func animes(router chi.Router) {
	router.Get("/", GetAllAnimes)
	router.Route("/{id}", func(router chi.Router) {
		router.Use(AnimeContext)
		router.Get("/", GetAnime)
		router.Delete("/", DeleteAnime)
		router.Put("/", UpdateAnime)
	})
	router.Route("/popularity/{n}", func(router chi.Router) {
		router.Use(PopularityContext)
		router.Get("/", GetMostPopularAnime)
	})
	router.Route("/genre", func(router chi.Router) {
		router.Get("/", GetAllAnimeByGenre)
	})
	router.Route("/recommendation/collaborative", func(router chi.Router) {
		router.Get("/", GetRecommendationCollaborative)
	})
	router.Route("/recommendation/content", func(router chi.Router) {
		router.Get("/", GetRecommendationContent)
	})
}

func health(router chi.Router) {
	router.Get("/", GetHealth)
}

func AnimeContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		animeId := chi.URLParam(r, "id")
		if animeId == "" {
			if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("anime ID is required"))); err != nil {
				log.Println(err)
			}
			return
		}
		ctx := context.WithValue(r.Context(), animeIDKey, animeId)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func PopularityContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		animen := chi.URLParam(r, "n")
		if animen == "" {
			if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("anime number is required"))); err != nil {
				log.Println(err)
			}
			return
		}
		ctx := context.WithValue(r.Context(), animeNKey, animen)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func GetHealth(w http.ResponseWriter, r *http.Request) {
	render.Status(r, http.StatusOK)
	render.JSON(w, r, map[string]string{"message": "Service is healthy.", "service": "anime"})
}

func GetAllAnimes(w http.ResponseWriter, r *http.Request) {
	animes, err := dbInstance.GetAllAnime()
	if err != nil {
		if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
			log.Println(err)
		}
		return
	}
	retAnimes := make([]*AnimeWithGenre, 0)
	for _, v := range animes {
		retAnimes = append(retAnimes, &AnimeWithGenre{Anime: *v, Genres: v.R.Genres})
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, retAnimes)
}

func GetAnime(w http.ResponseWriter, r *http.Request) {
	animeId, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("invalid Anime ID"))); err != nil {
			log.Println(err)
		}
		return
	}

	anime, err := dbInstance.GetAnimeByID(animeId)
	if err != nil {
		if err == db.ErrNoMatch {
			if err := render.Render(w, r, ErrNotFound); err != nil {
				log.Println(err)
			}
		} else {
			if err := render.Render(w, r, ErrorRenderer(err)); err != nil {
				log.Println(err)
			}
		}
		return
	}

	ret := AnimeWithGenre{
		Anime:  *anime,
		Genres: anime.R.Genres,
	}

	render.Status(r, http.StatusOK)
	render.JSON(w, r, &ret)
}

func UpdateAnime(w http.ResponseWriter, r *http.Request) {
	animeId, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("invalid Anime ID"))); err != nil {
			log.Println(err)
		}
		return
	}
	updateAnimeRequest := &models.UpdateAnime{}
	if err = render.Bind(r, updateAnimeRequest); err != nil {
		if err := render.Render(w, r, ErrBadRequest); err != nil {
			log.Println(err)
		}
		return
	}

	resAnime, err := dbInstance.UpdateAnime(animeId, updateAnimeRequest.Anime)
	if err != nil {
		if err == db.ErrNoMatch {
			if err := render.Render(w, r, ErrNotFound); err != nil {
				log.Println(err)
			}
		} else {
			if err := render.Render(w, r, ErrorRenderer(err)); err != nil {
				log.Println(err)
			}
		}
		return
	}

	render.Status(r, http.StatusOK)
	render.JSON(w, r, resAnime)
}

func DeleteAnime(w http.ResponseWriter, r *http.Request) {
	animeId, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("invalid Anime ID"))); err != nil {
			log.Println(err)
		}
		return
	}

	err = dbInstance.DeleteAnime(animeId)
	if err != nil {
		if err == db.ErrNoMatch {
			if err := render.Render(w, r, ErrNotFound); err != nil {
				log.Println(err)
			}
		} else {
			if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
				log.Println(err)
			}
		}
		return
	}
	render.DefaultResponder(w, r, "Successfully Deleted")
}

func GetMostPopularAnime(w http.ResponseWriter, r *http.Request) {
	n := r.Context().Value(animeNKey).(string)
	number, err := strconv.Atoi(n)
	if err != nil {
		if err := render.Render(w, r, ServerErrorRenderer(errors.New("invalid number"))); err != nil {
			log.Println(err)
		}
	}

	animes, err := dbInstance.GetMostPopularAnime(number)
	if err != nil {
		if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
			log.Println(err)
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, animes)

}

func GetRecommendationCollaborative(w http.ResponseWriter, r *http.Request) {
	n := r.URL.Query().Get("n")
	id := r.URL.Query().Get("id")
	reco := CollaborativeRecommendation{}

	recommenderConn, exists := os.LookupEnv("RECOMMENDER_HOST")
	if !exists {
		log.Fatalf("Missing RECOMMENDER_HOST environment variable")
	}

	response, err := http.Get(recommenderConn + "/recommendations/collaborative/" + id + ":" + n)
	if err != nil {
		fmt.Println(err.Error())
	}
	responseData, err := io.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
	}
	err = json.Unmarshal(responseData, &reco)
	if err != nil {
		log.Println(err)
	}

	animes, err := dbInstance.GetRecommendations(reco.Data.CollaborativeRecommendations)
	if err != nil {
		if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
			log.Println(err)
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, animes)

}

func GetRecommendationContent(w http.ResponseWriter, r *http.Request) {
	n := r.URL.Query().Get("n")
	id := r.URL.Query().Get("id")
	reco := ContentRecommendation{}

	recommenderConn, exists := os.LookupEnv("RECOMMENDER_HOST")
	if !exists {
		log.Fatalf("Missing RECOMMENDER_HOST environment variable")
	}

	response, err := http.Get(recommenderConn + "/recommendations/content/" + id + ":" + n)
	if err != nil {
		fmt.Println(err.Error())
	}
	responseData, err := io.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
	}
	err = json.Unmarshal(responseData, &reco)
	if err != nil {
		log.Println(err)
	}
	animes, err := dbInstance.GetRecommendations(reco.Data.CollaborativeRecommendations)
	if err != nil {
		if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
			log.Println(err)
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, animes)

}

func GetAllAnimeByGenre(w http.ResponseWriter, r *http.Request) {
	genre := r.URL.Query().Get("genre")
	animes, err := dbInstance.GetAllAnimeByGenre(genre)
	if err != nil {
		if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
			log.Println(err)
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, animes)
}
